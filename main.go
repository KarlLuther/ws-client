package main

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"net/url"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/gorilla/websocket"
)



func main() {
	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}

	u := url.URL{Scheme: "ws", Host: "localhost:8080", Path: "/ws"}
	log.Printf("connecting to %s", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}

	wg.Add(1)
	go func() {
		defer wg.Done()

		for {
			select { 
			case <-ctx.Done():
				log.Println("Context done, exiting reader goroutine")
				return
			default:
				_, message, err := c.ReadMessage()
				if err != nil {
					log.Println("Error reading message:", err)
					cancel()
					return
				}
				log.Printf("recv: %s", message)
			}
		}
	}()

	wg.Add(1)
	go func() {
		ticker := time.NewTicker(time.Minute * 10)
		defer ticker.Stop()
		defer wg.Done()

		for {
			select {
			case <-ctx.Done():
				log.Println("Context done, exiting heartbeat goroutine")
				return
			case t := <-ticker.C:
				err := c.WriteMessage(websocket.TextMessage, []byte(t.String()))
				if err != nil {
					log.Println("Error writing heartbeat:", err)
					cancel()
					return
				}
			}
		}
	}()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-sig

		log.Println("interrupt received, closing connection")
		_ = c.WriteMessage(websocket.CloseMessage,
			websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""),
		)
		_ = c.Close()

		cancel()
		fmt.Println("Connection closed! Press any key...")
	}()

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text := scanner.Text()
		if err = c.WriteMessage(websocket.TextMessage, []byte(text)); err != nil {
			log.Println("Error writing message:", err)
			break
		}
	}

	wg.Wait()
}
